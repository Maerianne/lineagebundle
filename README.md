LineageBundle
=================
[![Latest Stable Version](https://poser.pugx.org/maesbox/lineagebundle/v/stable.svg)](https://packagist.org/packages/maesbox/lineagebundle) [![Total Downloads](https://poser.pugx.org/maesbox/lineagebundle/downloads.svg)](https://packagist.org/packages/maesbox/lineagebundle) [![Latest Unstable Version](https://poser.pugx.org/maesbox/lineagebundle/v/unstable.svg)](https://packagist.org/packages/maesbox/lineagebundle) [![License](https://poser.pugx.org/maesbox/lineagebundle/license.svg)](https://packagist.org/packages/maesbox/lineagebundle)

Présentation 
--------------------

Ce bundle permet la gestion d'un serveur lineage 2.

Installation
--------------------

- installation avec composer

~~~~
"require": {
    ...
    "maesbox/lineagebundle": "dev-master",
    ...
},
~~~~


Configuration
--------------------

- dans le fichier "app/routing.yml" :

~~~~

~~~~

- dans le fichier "app/parameters.yml" :

~~~~

~~~~